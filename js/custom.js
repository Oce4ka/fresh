////////////////////
// Parallax Icons //
////////////////////

var optionsParallax = {
  /*
    relativeInput: false,
    clipRelativeInput: false,
    calibrationThreshold: 100,
    calibrationDelay: 500,
    supportDelay: 500,
    calibrateX: true,
    calibrateY: false,
    invertX: true,
    invertY: true,
    limitX: false,
    limitY: false,
    scalarX: 5.0,
    scalarY: 5.0,
    frictionX: 0.1,
    frictionY: 0.1,
    originX: 0.5,
    originY: 0.5,
    //hoverOnly: true
    */
};


var parallaxArrayIDs = ['iconsParallaxHome', 'iconsParallaxAbout', 'iconsParallaxStaff', 'iconsParallaxPartners', 'iconsParallaxContact', 'iconsParallaxPage', 'iconsParallaxMenu'];
for (i = 0; i < parallaxArrayIDs.length; i++) {
  if ($('#' + parallaxArrayIDs[i]).length) new Parallax(document.getElementById(parallaxArrayIDs[i]), optionsParallax);
}

///////////////////////////////////////////////////////
////////////////////// Carousel ///////////////////////
///////////////////////////////////////////////////////

$('.slick-slider').slick({
  centerMode: true,
  slidesToShow: 3,
  variableWidth: true,
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [
    {
      breakpoint: 813,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});

// section-services
let slickOptionsListServices = {
  responsive: [
    {
      breakpoint: 99999,
      settings: "unslick"
    },
    {
      breakpoint: 814,
      settings: {
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        autoplaySpeed: 3000,
      }
    }
  ]
};

slickListServices = $('.list-services').slick(slickOptionsListServices);

$(window).on("resize", function () {
  if ($('html, body').width() < 814) {
    slickListServices.not('.slick-initialized').slick(slickOptionsListServices)
  }
});

// section-staff
let slickOptionsListStaff = {
  slidesToShow: 5,
  centerMode: true,
  variableWidth: true,
  autoplaySpeed: 3000,
  responsive: [
    {
      breakpoint: 99999,
      settings: "slick"
    },
    {
      breakpoint: 814,
      settings: "unslick",
    }
  ]
};

slickListStaff = $('.list-staff').slick(slickOptionsListStaff);

$(window).on("resize", function () {
  if (($('.item-staff').length > 5) && ($('html, body').width() >= 814)) {
    slickListStaff.not('.slick-initialized').slick(slickOptionsListStaff);
  }
});

////////////////////////////////////////////////
///////////// Fixed Contact Form ///////////////
////////////////////////////////////////////////

$('.contact-trigger, [href="#modalContactForm"]').click(function (e) {
  $('.contact-form-fixed').addClass('open');
  $('.nav-main .close').click();
  $('.menu-main').toggleClass('open');
  $('.hamburger').toggleClass('close');
  return false;
})

$('.contact-form-fixed .close').click(function () {
  $('.contact-form-fixed').removeClass('open');
  return false;
})

///////////////////////////////////////////////////
///////////// Mobile menu Hamburger ///////////////
///////////////////////////////////////////////////

$('.hamburger').click(function (e) {
  $('.menu-main').toggleClass('open');
  $('.hamburger').toggleClass('close');
  $('#iconsParallaxMenu').toggleClass('showed');
  return false;
})

$('.menu-main a').click(function () {
  $('.menu-main').toggleClass('open');
  $('.hamburger').toggleClass('close');
  $('#iconsParallaxMenu').toggleClass('showed');
})

///////////////////////////////////////////////////
///////////// Mobile show more stuff button ///////////////
///////////////////////////////////////////////////

$('#showMoreStaff').click(function (e) {
  e.preventDefault;
  $('.item-staff').not(':visible').first().show();
  if (!$('.item-staff').not(':visible').length) {
    $('#showMoreStaff').hide();
  }
  return false;
})

$('#showMoreLogos').click(function (e) {
  e.preventDefault;
  $('.item-logos').not(':visible').first().css('display', 'flex');
  $('.item-logos').not(':visible').first().css('display', 'flex');
  if (!$('.item-logos').not(':visible').length) {
    $('#showMoreLogos').hide();
  }
  return false;
})


///////////////////////////////////////////////////////
////////////////////// Animation //////////////////////
///////////////////////////////////////////////////////

// init controller
var controller = new ScrollMagic.Controller();

// build scenes

// Main menu is going black
new ScrollMagic.Scene({
  triggerElement: "#section-home",
  offset: ($('#section-home').height() / 4) + ($('#viewport').height() / 2),
})
  .setClassToggle(".nav-main", "semifixed") // add class toggle fixed
  .on("enter", function () {
    $('.arrows-up').addClass('current');
  })
  .on("leave", function () {
    $('.arrows-up').removeClass('current');
  })
  //.addIndicators({name: "menu is going black"}) // add indicators (requires plugin)
  .addTo(controller);

if ($('#section-bag').length) {

  // Main menu logo appearing
  new ScrollMagic.Scene({
    triggerElement: "#section-home",
    offset: ($('#section-home').height() / 2) + ($('#viewport').height() / 2),
  })
    .setClassToggle(".nav-main", "fixed") // add class toggle
    //.addIndicators({name: "menu is with logo"}) // add indicators (requires plugin)
    .addTo(controller);

  // About Section - Characters appearing
  var sceneAboutCharacters = new ScrollMagic.Scene({
    //offset: $('#section-about').offset().top,
    triggerElement: "#section-about",
    duration: $('html, body').height() - $('#section-about').offset().top,
  })
    .setClassToggle(".couple-girl, .couple-boy, .couple-shadow", "current") // add class toggle
    //.addIndicators({name: "characters"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    sceneAboutCharacters.duration($('#section-about').height());
  });

  // Home section
  var sceneHome = new ScrollMagic.Scene({
    triggerElement: "#section-home",
    duration: $('#section-home').height(),
  })
    .setClassToggle(".leaf-home", "current") // add class toggle
    //.addIndicators({name: "#section-home"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    sceneHome.duration($('#section-home').height());
  });

// Section About
  sceneAbout = new ScrollMagic.Scene({
    triggerElement: "#section-about",
    duration: $('#section-about').height(),
  })
    .setClassToggle(".leaf-about, .menu-item-about", "current") // add class toggle
    //.addIndicators({name: "#section-about"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
      sceneAbout.duration($('#section-about').height());
    }
  );

// Section Services
  sceneServices = new ScrollMagic.Scene({
    triggerElement: "#section-services",
    duration: $('#section-services').height(),
  })
    .setClassToggle(".leaf-services, .menu-item-services", "current") // add class toggle
    //.addIndicators({name: "#section-services"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    sceneServices.duration($('#section-services').height());
  });

// Section Staff
  sceneStaff = new ScrollMagic.Scene({
    triggerElement: "#section-staff",
    duration: $('#section-staff').height(),
  })
    .setClassToggle(".leaf-staff, .menu-item-staff", "current") // add class toggle
    //.addIndicators({name: "#section-staff"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    sceneStaff.duration($('#section-staff').height());
  });

// Section Bag
  sceneBag = new ScrollMagic.Scene({
    triggerElement: "#section-bag",
    duration: $('#section-bag').height(),
  })
    .setClassToggle(".leaf-bag, .menu-item-bag", "current") // add class toggle
    //.addIndicators({name: "#section-bag"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    sceneBag.duration($('#section-bag').height());
  });

  // Section Partners
  scenePartners = new ScrollMagic.Scene({
    triggerElement: "#section-partners",
    duration: $('#section-partners').height(),
  })
    .setClassToggle(".leaf-partners", "current") // add class toggle
    //.addIndicators({name: "#section-partners"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    scenePartners.duration($('#section-partners').height());
  });

  // Section Contact
  sceneContact = new ScrollMagic.Scene({
    triggerElement: "#section-contact",
    duration: $('#section-contact').height(),
  })
    .setClassToggle(".leaf-contact", "current") // add class toggle
    //.addIndicators({name: "#section-contact"}) // add indicators (requires plugin)
    .addTo(controller);
  $(window).on("resize", function () {
    sceneContact.duration($('#section-contact').height());
  });

} else if ($('#section-page-content').length) {
  $('.menu-item-bag').addClass("current");
  
  // Main menu logo appearing
  new ScrollMagic.Scene({
    triggerElement: "#section-home",
    offset: ($('#section-home').height() / 2) + ($('#viewport').height() / 2),
  })
    .setClassToggle(".nav-main", "fixed") // add class toggle
    //.addIndicators({name: "menu is with logo"}) // add indicators (requires plugin)
    .addTo(controller);
}

// Dots
const dotEffectCount = 6;

var arrayDotsRandom = [];

for (m = 1; m <= dotEffectCount; m++) {
  arrayDotsRandom[m] = [];
  for (n = 1; n <= dotsCount; n++) {
    arrayDotsRandom[m].push(n);
  }
  shuffle(arrayDotsRandom[m]);
}
var sceneDots = [];
var dotsAppearingStepsCount = 20;

var dotEffectOptions = [];
// Above About
dotEffectOptions[1] = {
  triggerElement: "#section-home",
  section: $('#section-about').length ? $('#section-about') : $('#section-home'),
};
// Above Services
dotEffectOptions[2] = {
  triggerElement: "#section-about",
  section: $('#section-services'),
};
// Above Staff
dotEffectOptions[3] = {
  triggerElement: "#section-services",
  section: $('#section-staff'),
};
// Above Bag
dotEffectOptions[4] = {
  triggerElement: "#section-staff",
  section: $('#section-bag'),
};
// Above Partners
dotEffectOptions[5] = {
  triggerElement: "#section-bag",
  section: $('#section-partners'),
};


for (m = 1; m <= (dotEffectCount - 1); m++) { //dotEffectCount
  sceneDots[m] = [];
  for (i = 1; i <= dotsAppearingStepsCount; i++) {

    var selectorDots = '';
    for (j = 0; j < Math.ceil((dotsCount / dotsAppearingStepsCount)); j++) {
      if (arrayDotsRandom[m].length) {
        selectorDots = selectorDots + dotEffectOptions[m].triggerElement + " .dot:nth-child(" + arrayDotsRandom[m].pop() + "), ";
      }
    }
    selectorDots = selectorDots.substring(0, selectorDots.length - 2);

    sceneDots[m][i] = new ScrollMagic.Scene({
      triggerElement: dotEffectOptions[m].triggerElement,
      offset: ($('#viewport').height() / 2) + ((dotEffectOptions[m].section.height() / dotsAppearingStepsCount) * i),
    })
    ////.addIndicators({name: "Green dots " + m + "   " + i}) // add indicators (requires plugin)
      .setClassToggle(selectorDots, "visible") // add class toggle
      .addTo(controller);
  }
}

// Above Contact
dotEffectOptions[6] = {
  triggerElement: $('#section-partners').length ? "#section-partners" : "#section-page-content",
  section: $('#section-partners').length ? $('#section-page-content') : $('#section-partners'),
};


m = 6; //dotEffectCount
sceneDots[m] = [];
for (i = 1; i <= dotsAppearingStepsCount; i++) {

  var selectorDots = '';
  for (j = 0; j < Math.ceil((dotsCount / dotsAppearingStepsCount)); j++) {
    if (arrayDotsRandom[m].length) {
      selectorDots = selectorDots + dotEffectOptions[m].triggerElement + " .dot:nth-child(" + arrayDotsRandom[m].pop() + "), ";
    }
  }
  selectorDots = selectorDots.substring(0, selectorDots.length - 2);

  sceneDots[m][i] = new ScrollMagic.Scene({
    offset: $('#section-contact').offset().top - $('#viewport').height() + (($('#viewport').height() / 2 / dotsAppearingStepsCount) * i),
    duration: $('body').height() - $('#section-contact').offset().top - $('#viewport').height() + (($('#viewport').height() / 2) * i),
  })
  ////.addIndicators({name: "Green dots " + m + "   " + i}) // add indicators (requires plugin)
    .setClassToggle(selectorDots, "visible") // add class toggle
    .addTo(controller);
}


// Shuffle Array fuction

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

jQuery(document).ready(function(){
	// init Isotope
	// TODO: fixed by Marina, need to be checked if all is right by Alex
	if ($('.portfolio-list').length) {
	  var $grid = $('.portfolio-list').isotope({
		itemSelector: '.portfolio-item',
		layoutMode: 'fitRows',
		isOriginLeft: false
	  });
	}

	$('#filters ul li').on("click", function () {
	  var value = $(this).attr('data-filter');
	  $grid.isotope({
		filter: value
	  });
	});

	// change is-checked class on buttons
	$('#filters').each(function (i, buttonGroup) {
	  var $buttonGroup = $(buttonGroup);
	  $buttonGroup.on('click', 'li', function () {
		if ($(window).width() <= '799' && $(this).hasClass('is-checked')) {
		  $('#filters ul li').toggleClass('opened closed');
		} else {
		  $buttonGroup.find('.is-checked').removeClass('is-checked');
		  $(this).addClass('is-checked');

		  if ($(window).width() <= '799') {
			$('#filters ul li').toggleClass('opened closed');
		  }
		}
	  });
	});
});
// portfoli page slider
if ($('.slick-portfolio-page').length) {
  $('.slick-portfolio-page').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    prevArrow: $('.simple-left-arrow'),
    nextArrow: $('.simple-right-arrow'),
  });
}

// Add smooth scrolling to all links
$("a").on('click', function (event) {

  // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "" && this.hash !== "#show-more") {
    // Prevent default anchor click behavior
	if ($(this).hasClass("link") == "false") {
		event.preventDefault();
	}

    // Store hash
    var hash = this.hash;
    console.log(event.target);

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 800, function () {

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
  } // End if
});

$('.slick-slider').find('.slick-prev, .slick-next, .simple-right-arrow, .simple-left-arrow').html('<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 48 39.7">\n' +
  '    <switch>\n' +
  '        <g>\n' +
  '            <g class="svg-arrow" fill-rule="evenodd" clip-rule="evenodd" fill="#001c14">\n' +
  '                <path d="M23.75 0H32v3.2h3v4.7h5v3.3h5v4.5h3v8.25H0V15.7h31.75v-4.25h-5v-3.2h-3z"/>\n' +
  '                <path d="M23.75 39.7v-8.25h3v-3.2h5V24H0v-8.25h48V24h-3v4.5h-5v3.3h-5v4.7h-3v3.2z"/>\n' +
  '            </g>\n' +
  '        </g>\n' +
  '    </switch>\n' +
  '</svg>\n')